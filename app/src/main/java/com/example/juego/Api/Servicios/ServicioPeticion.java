package com.example.juego.Api.Servicios;

import com.example.juego.Api.ViewModels.ObtenerNumero;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ServicioPeticion {


    @POST("api/numAlea")
    Call<ObtenerNumero> EXTRAERNUMEROS_CALL();
}
