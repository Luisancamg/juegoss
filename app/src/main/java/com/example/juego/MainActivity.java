package com.example.juego;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juego.Api.Servicios.ServicioPeticion;
import com.example.juego.Api.ViewModels.ObtenerNumero;
import com.example.juego.Api.api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView textV;
    private EditText editT;
    private Button btnA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       textV = (TextView) findViewById(R.id.textV);
       btnA = (Button) findViewById(R.id.btnA);
       editT = (EditText) findViewById(R.id.editT);

       textV.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ServicioPeticion service = api.getApi(MainActivity.this).create(ServicioPeticion.class);
               Call<ObtenerNumero> registrarCall = service.EXTRAERNUMEROS_CALL();
               registrarCall.enqueue(new Callback<ObtenerNumero>() {
                   @Override
                   public void onResponse(Call<ObtenerNumero> call, Response<ObtenerNumero> response) {
                       ObtenerNumero peticion = response.body();
                       int num1 = Integer.parseInt(peticion.num1.toString());
                       textV.setText(peticion.num1.toString());
                       int num2 = Integer.parseInt(peticion.num2.toString());
                       textV.setText((peticion.num2.toString()));
                   }


                   @Override
                   public void onFailure(Call<ObtenerNumero> call, Throwable t) {
                       Toast.makeText(MainActivity.this,"USTED SE A EQUIVOCADO INTENTE DE NUEVO", Toast.LENGTH_LONG).show();


                   }
               });
           }
       });
       editT.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               resultado = n1 + n2;
           }
       });
        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editT = resultado){
                    Toast.makeText(MainActivity.this, "Usted si sabe sumar felicidades", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
    